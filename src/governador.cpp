#include "governador.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

Governador::Governador(){
  nome_completo = "";
  nome_vice = "";
  nome_urna = "";
  cargo = "";
  sigla_partido = "";
  nome_partido = "";
  numero_partido = "";
  numero_candidato = "";
}

Governador::~Governador(){};

string Governador::get_nome_completo(){
  return nome_completo;
}
void Governador::set_nome_completo(string nome_completo){
  this->nome_completo = nome_completo;
}
string Governador::get_nome_vice(){
  return nome_vice;
}
void Governador::set_nome_vice(string nome_vice){
  this->nome_vice = nome_vice;
}
string Governador::get_nome_urna(){
  return nome_urna;
}
void Governador::set_nome_urna(string nome_urna){
  this->nome_urna = nome_urna;
}
string Governador::get_cargo(){
  return cargo;
}
void Governador::set_cargo(string cargo){
  this->cargo = cargo;
}
string Governador::get_sigla_partido(){
  return sigla_partido;
}
void Governador::set_sigla_partido(string sigla_partido){
  this->sigla_partido = sigla_partido;
}
string Governador::get_nome_partido(){
  return nome_partido;
}
void Governador::set_nome_partido(string nome_partido){
  this->nome_partido = nome_partido;
}
string Governador::get_numero_partido(){
  return numero_partido;
}
void Governador::set_numero_partido(string numero_partido){
  this->numero_partido = numero_partido;
}
string Governador::get_numero_candidato(){
  return numero_candidato;
}
void Governador::set_numero_candidato(string numero_candidato){
  this->numero_candidato = numero_candidato;
}
void Governador::imprime_dados_gov(){
  string num;
  string aux1 = "8";
  string aux2 = "4";

  ifstream ip("../data/consulta_cand_2018_DF-melhorada.csv");

  if(!ip.is_open()) cout << "ERROR: File Open" << endl;

  string gov[1238][8];

  for(int i = 0; i < 1238; i++){
    for(int j = 0; j < 8; j++){
      if(j == 7){
        getline(ip, gov[i][j], '\n');
      }
      else{
        getline(ip, gov[i][j], ',');
      }
    }
  }

  num = get_numero_candidato();

  for(int i = 0; i < 1238; i++){
    for(int j = 0; j < 8; j++){
      if(num == gov[i][2] && aux1 == gov[i][0]){
        set_nome_completo(gov[i][3]);
        set_nome_urna(gov[i][4]);
        set_numero_partido(gov[i][5]);
        set_sigla_partido(gov[i][6]);
        set_nome_partido(gov[i][7]);
      }
      if(num == gov[i][2] && aux2 == gov[i][0]){
        set_nome_vice(gov[i][4]);
      }
    }
  }

  cout << "Nome: " << endl << get_nome_urna() << endl;
  cout << "Partido: " << endl << get_nome_partido() << " (" << get_sigla_partido() << ")" << endl;
  cout << get_numero_partido() << endl;
  cout << "Vice: " << endl << get_nome_vice() << endl;

  ip.close();
}
