#include "deputado_fedederal.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

Deputado_Federal::Deputado_Federal(){
  nome_completo = "";
  nome_urna = "";
  cargo = "";
  sigla_partido = "";
  nome_partido = "";
  numero_partido = "";
  numero_candidato = "";
}

Deputado_Federal::~Deputado_Federal(){};

string Deputado_Federal::get_nome_completo(){
  return nome_completo;
}
void Deputado_Federal::set_nome_completo(string nome_completo){
  this->nome_completo = nome_completo;
}
string Deputado_Federal::get_nome_urna(){
  return nome_urna;
}
void Deputado_Federal::set_nome_urna(string nome_urna){
  this->nome_urna = nome_urna;
}
string Deputado_Federal::get_cargo(){
  return cargo;
}
void Deputado_Federal::set_cargo(string cargo){
  this->cargo = cargo;
}
string Deputado_Federal::get_sigla_partido(){
  return sigla_partido;
}
void Deputado_Federal::set_sigla_partido(string sigla_partido){
  this->sigla_partido = sigla_partido;
}
string Deputado_Federal::get_nome_partido(){
  return nome_partido;
}
void Deputado_Federal::set_nome_partido(string nome_partido){
  this->nome_partido = nome_partido;
}
string Deputado_Federal::get_numero_partido(){
  return numero_partido;
}
void Deputado_Federal::set_numero_partido(string numero_partido){
  this->numero_partido = numero_partido;
}
string Deputado_Federal::get_numero_candidato(){
  return numero_candidato;
}
void Deputado_Federal::set_numero_candidato(string numero_candidato){
  this->numero_candidato = numero_candidato;
}
void Deputado_Federal::imprime_dados_fed(){
  string num;
  string aux1 = "6";

  ifstream ip("../data/consulta_cand_2018_DF-melhorada.csv");

  if(!ip.is_open()) cout << "ERROR: File Open" << endl;

  string dep_fed[1238][8];

  for(int i = 0; i < 1238; i++){
    for(int j = 0; j < 8; j++){
      if(j == 7){
        getline(ip, dep_fed[i][j], '\n');
      }
      else{
        getline(ip, dep_fed[i][j], ',');
      }
    }
  }

  num = get_numero_candidato();

  for(int i = 0; i < 1238; i++){
    for(int j = 0; j < 8; j++){
      if(num == dep_fed[i][2] && aux1 == dep_fed[i][0]){
        set_nome_completo(dep_fed[i][3]);
        set_nome_urna(dep_fed[i][4]);
        set_numero_partido(dep_fed[i][5]);
        set_sigla_partido(dep_fed[i][6]);
        set_nome_partido(dep_fed[i][7]);
      }
    }
  }

  cout << "Nome do Candidato: " << endl << get_nome_urna() << endl;
  cout << "Partido " << endl << get_nome_partido() << " (" << get_sigla_partido() << ")" << endl;
  cout << get_numero_partido() << endl;

  ip.close();
}
