#include "deputado_distrital.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

Deputado_Distrital::Deputado_Distrital(){
  nome_completo = "";
  nome_urna = "";
  cargo = "";
  sigla_partido = "";
  nome_partido = "";
  numero_partido = "";
  numero_candidato = "";
}

Deputado_Distrital::~Deputado_Distrital(){};

string Deputado_Distrital::get_nome_completo(){
  return nome_completo;
}
void Deputado_Distrital::set_nome_completo(string nome_completo){
  this->nome_completo = nome_completo;
}
string Deputado_Distrital::get_nome_urna(){
  return nome_urna;
}
void Deputado_Distrital::set_nome_urna(string nome_urna){
  this->nome_urna = nome_urna;
}
string Deputado_Distrital::get_cargo(){
  return cargo;
}
void Deputado_Distrital::set_cargo(string cargo){
  this->cargo = cargo;
}
string Deputado_Distrital::get_sigla_partido(){
  return sigla_partido;
}
void Deputado_Distrital::set_sigla_partido(string sigla_partido){
  this->sigla_partido = sigla_partido;
}
string Deputado_Distrital::get_nome_partido(){
  return nome_partido;
}
void Deputado_Distrital::set_nome_partido(string nome_partido){
  this->nome_partido = nome_partido;
}
string Deputado_Distrital::get_numero_partido(){
  return numero_partido;
}
void Deputado_Distrital::set_numero_partido(string numero_partido){
  this->numero_partido = numero_partido;
}
string Deputado_Distrital::get_numero_candidato(){
  return numero_candidato;
}
void Deputado_Distrital::set_numero_candidato(string numero_candidato){
  this->numero_candidato = numero_candidato;
}
void Deputado_Distrital::imprime_dados_dis(){
  string num;
  string aux1 = "8";

  ifstream ip("../data/consulta_cand_2018_DF-melhorada.csv");

  if(!ip.is_open()) cout << "ERROR: File Open" << endl;

  string dep_dis[1238][8];

  for(int i = 0; i < 1238; i++){
    for(int j = 0; j < 8; j++){
      if(j == 7){
        getline(ip, dep_dis[i][j], '\n');
      }
      else{
        getline(ip, dep_dis[i][j], ',');
      }
    }
  }

  num = get_numero_candidato();

  for(int i = 0; i < 1238; i++){
    for(int j = 0; j < 8; j++){
      if(num == dep_dis[i][2] && aux1 == dep_dis[i][0]){
        set_nome_completo(dep_dis[i][3]);
        set_nome_urna(dep_dis[i][4]);
        set_numero_partido(dep_dis[i][5]);
        set_sigla_partido(dep_dis[i][6]);
        set_nome_partido(dep_dis[i][7]);
      }
    }
  }

  cout << "Nome do Candidato: " << endl << get_nome_urna() << endl;
  cout << "Partido " << endl << get_nome_partido() << " (" << get_sigla_partido() << ")" << endl;
  cout << get_numero_partido() << endl;

  ip.close();
}
