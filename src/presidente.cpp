#include "presidente.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

Presidente::Presidente(){
  nome_completo = "";
  nome_vice = "";
  nome_urna = "";
  cargo = "";
  sigla_partido = "";
  nome_partido = "";
  numero_partido = "";
  numero_candidato = "";
}

Presidente::~Presidente(){};

string Presidente::get_nome_completo(){
  return nome_completo;
}
void Presidente::set_nome_completo(string nome_completo){
  this->nome_completo = nome_completo;
}
string Presidente::get_nome_vice(){
  return nome_vice;
}
void Presidente::set_nome_vice(string nome_vice){
  this->nome_vice = nome_vice;
}
string Presidente::get_nome_urna(){
  return nome_urna;
}
void Presidente::set_nome_urna(string nome_urna){
  this->nome_urna = nome_urna;
}
string Presidente::get_cargo(){
  return cargo;
}
void Presidente::set_cargo(string cargo){
  this->cargo = cargo;
}
string Presidente::get_sigla_partido(){
  return sigla_partido;
}
void Presidente::set_sigla_partido(string sigla_partido){
  this->sigla_partido = sigla_partido;
}
string Presidente::get_nome_partido(){
  return nome_partido;
}
void Presidente::set_nome_partido(string nome_partido){
  this->nome_partido = nome_partido;
}
string Presidente::get_numero_partido(){
  return numero_partido;
}
void Presidente::set_numero_partido(string numero_partido){
  this->numero_partido = numero_partido;
}
string Presidente::get_numero_candidato(){
  return numero_candidato;
}
void Presidente::set_numero_candidato(string numero_candidato){
  this->numero_candidato = numero_candidato;
}
void Presidente::imprime_dados_pre(){
  string num;
  string aux1 = "1";
  string aux2 = "2";

  ifstream ip("../data/consulta_cand_2018_BR-melhorada.csv");

  if(!ip.is_open()) cout << "ERROR: File Open" << endl;

  string pre[27][7];

  for(int i = 0; i < 27; i++){
    for(int j = 0; j < 7; j++){
      if(j == 6){
        getline(ip, pre[i][j], '\n');
      }
      else{
        getline(ip, pre[i][j], ',');
      }
    }
  }

  num = get_numero_candidato();

  for(int i = 0; i < 27; i++){
    for(int j = 0; j < 7; j++){
      if(num == pre[i][2] && aux1 == pre[i][0]){
        set_nome_completo(pre[i][3]);
        set_nome_urna(pre[i][4]);
        set_sigla_partido(pre[i][5]);
        set_nome_partido(pre[i][6]);
      }
      if(num == pre[i][2] && aux2 == pre[i][0]){
        set_nome_vice(pre[i][4]);
      }
    }
  }

  cout << "Nome: " << endl << get_nome_urna() << endl;
  cout << "Partido: " << endl << get_nome_partido() << " (" << get_sigla_partido() << ")" << endl;
  cout << get_numero_partido() << endl;
  cout << "Vice: " << endl << get_nome_vice() << endl;

  ip.close();
}
