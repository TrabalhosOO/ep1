#ifndef DEPUTADO_FEDERAL_HPP
#define DEPUTADO_FEDERAL_HPP

#include <string>

using namespace std;

class Deputado_Federal{

private:

  string nome_completo;
  string nome_urna;
  string cargo;
  string sigla_partido;
  string nome_partido;
  string numero_partido;
  string numero_candidato;

public:

  Deputado_Federal();
  ~Deputado_Federal();
  string get_nome_completo();
    void set_nome_completo(string nome_completo);
  string get_nome_urna();
    void set_nome_urna(string nome_urna);
  string get_cargo();
    void set_cargo(string cargo);
  string get_sigla_partido();
    void set_sigla_partido(string sigla_partido);
  string get_nome_partido();
    void set_nome_partido(string nome_partido);
  string get_numero_partido();
    void set_numero_partido(string numero_partido);
  string get_numero_candidato();
    void set_numero_candidato(string numero_candidato);
  void imprime_dados_fed();
};

#endif
