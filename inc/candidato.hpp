#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP

#include <string>

using namespace std;

class Candidato{
private:
  string nome;
  string cargo;
  string sigla_partido;
  string nome_partido;
  string numero;
public:
  Candidato();
  ~Candidato();
  string get_nome();
    void set_nome(string nome);
  string get_cargo();
    void set_cargo(string cargo);
  string get_sigla_partido();
    void set_sigla_partido(string sigla_partido);
  string get_nome_partido();
    void set_nome_partido(string nome_partido);
  string get_numero();
    void set_numero(string numero);
};
#endif
