#ifndef PRESIDENTE_HPP
#define PRESIDENTE_HPP

#include <string>

using namespace std;

class Presidente{
private:
  string nome_completo;
  string nome_vice;
  string nome_urna;
  string cargo;
  string sigla_partido;
  string nome_partido;
  string numero_partido;
  string numero_candidato;
public:
  Presidente();
  ~Presidente();
  string get_nome_completo();
    void set_nome_completo(string nome_completo);
  string get_nome_vice();
    void set_nome_vice(string nome_vice);
  string get_nome_urna();
    void set_nome_urna(string nome_urna);
  string get_cargo();
    void set_cargo(string cargo);
  string get_sigla_partido();
    void set_sigla_partido(string sigla_partido);
  string get_nome_partido();
    void set_nome_partido(string nome_partido);
  string get_numero_partido();
    void set_numero_partido(string numero_partido);
  string get_numero_candidato();
    void set_numero_candidato(string numero_candidato);
  void imprime_dados_pre();
};

#endif
